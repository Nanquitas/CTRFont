#include <3ds.h>
#include <citro2d.h>

#include <CTRFont.hpp>

#include <cstdarg>
#include <cstdlib>
#include <vector>

static C3D_RenderTarget* top;
static C3D_RenderTarget* bottom;

std::string     Format(const char* fmt, ...)
{
    char        buffer[0x100] = { 0 };
    va_list     argList;

    va_start(argList, fmt);
    vsnprintf(buffer, sizeof(buffer), fmt, argList);
    va_end(argList);

    return (std::string(buffer));
}

// Okay, it's not really a console's printf, but it fits our needs :p
void    Printf(const char *str, ...)
{
    char        buffer[0x100] = { 0 };
    va_list     argList;

    va_start(argList, str);
    vsnprintf(buffer, sizeof(buffer), str, argList);
    va_end(argList);

    C3D_FrameBegin(C3D_FRAME_SYNCDRAW);

    // Render top screen
    C2D_TargetClear(top, C2D_Color32(0x68, 0xB0, 0xD8, 0xFF));
    C2D_SceneBegin(top);

    Text(buffer).SetSize(0.5f, 0.75f).Draw(0, 0);

    // Render bottom screen
    C2D_TargetClear(bottom, C2D_Color32(0x68, 0xB0, 0xD8, 0xFF));
    C2D_SceneBegin(bottom);

    C3D_FrameEnd(0);
}

u64     Seconds(float sec)
{
    return sec * (float)SYSCLOCK_ARM11;
}

float   AsSeconds(u64 time)
{
    return (float)time / (float)SYSCLOCK_ARM11;
}

class Clock
{
public:

    static u64  GetCurrentTime(void)
    {
        return  svcGetSystemTick();
    }

    Clock(void)
    {
        start = GetCurrentTime();
    }

    u64     Restart(void)
    {
        u64 time = svcGetSystemTick();
        u64 elapsed = time - start;

        start = time;
        return elapsed;
    }

    u64     start;
};

class AnimatedText : public Text
{
public:
    AnimatedText(const std::string &src) : Text(src) {}

    AnimatedText(const std::string &src, const FontHandle &font) : Text(src, font) {}

    AnimatedText(const Color &color, const std::string &src, float scaleX, float scaleY, const FontHandle &font) :
        Text(color, src, scaleX, scaleY, font), _scaleXX(scaleX), _scaleYY(_scaleY)
    {}

    ~AnimatedText(void){}

    AnimatedText&   SetSize(float scaleX, float scaleY)
    {
        scaleX *= _font->GetTextScale();
        scaleY *= _font->GetTextScale();

        if (scaleX != _scaleXX || scaleY != _scaleYY)
        {
            _scaleX = _scaleXX = scaleX;
            _scaleY = _scaleYY = scaleY;
            _posX = _posXX;
            _posY = _posYY;
            _mode = true;
            _modifier = 0.f;
        }

        return *this;
    }

    AnimatedText&   SetPos(float posX, float posY)
    {
        _posX = _posXX = posX;
        _posY = _posYY = posY;

        return *this;
    }

    float    Draw(bool atBaseline = false)
    {
        float   modifier = 0.1f;
        float   delta = AsSeconds(_clock.Restart());
        float   xDelta = GetWidth();

        if (delta > 0.1f)
        {
            _mode = true;
            delta = 0.f;
            _posX = _posXX;
            _scaleX = _scaleXX;
            _scaleY = _scaleYY;
        }
        else
            delta *= modifier;


        if (_mode)
        {
            _scaleX += _scaleXX * delta;
            _scaleY += _scaleYY * delta;

            xDelta = (GetWidth() - xDelta) / 2.f;
            _posX -= xDelta;

            _modifier += delta;

            if (_modifier >= modifier)
                _mode = false;
        }
        else
        {
            _scaleX -= _scaleXX * delta;
            _scaleY -= _scaleYY * delta;

            xDelta = (xDelta - GetWidth()) / 2.f;
            _posX += xDelta;

            _modifier -= delta;

            if (_modifier <= 0.f)
                _mode = true;
        }

        return Text::Draw(atBaseline);
    }

private:

    float   _scaleXX;
    float   _scaleYY;
    float   _posXX;
    float   _posYY;
    float   _modifier{0.f};
    bool    _mode{true};
    Clock   _clock;
};

// Because I'm lazy
class FontContext
{
public:

    FontContext(const std::string &name, const FontHandle &font) :
        font(font), name(name)
    {}

    FontContext(const std::string &name) : name(name)
    {
        font = Font::Open("romfs:/" + name);
    }

    ~FontContext()
    {
        for (Text *text: texts)
            delete text;
    }

    void    operator+=(Text *text)
    {
        texts.push_back(text);
    }

    FontHandle  font;
    std::string name;

    std::vector<Text *>     texts;
};

std::vector<FontContext *> ctxs;
auto currentCtx = ctxs.begin();

static const char teststring[] =
    "Hello World - now with citro2d!\n"
    "The quick brown fox jumped over the lazy dog.\n"
    "\n"
    "En français: Vous ne devez pas éteindre votre console.\n"
    "日本語文章を見せるのも出来ますよ。\n"
    "Un poco de texto en español nunca queda mal.\n"
    "Πού είναι η τουαλέτα;\n"
    "Я очень рад, ведь я, наконец, возвращаюсь домой\n";

float boundsLeft = 50.f;
float boundsTop = 10.f;
float boundsWidth = 300.f;
float boundsHeight = 160.f;

static void sceneInit(void)
{
    auto initCtxs = [](const std::string &name)
    {
        Printf("Loading: %s ...\n", name.c_str());

        FontContext *ctx = name == "sysfont" ? new FontContext(name, Font::GetSysfont()) : new FontContext(name);

        if (!ctx || ctx->font->IsLoaded())
        {
            Printf("An error occured: %d\n", ctx->font->IsLoaded());
            return;
        }

        *ctx += new Text(teststring, ctx->font);
        *ctx += new AnimatedText(Color::Red, "I am red skinny text!", 0.5f, 0.75f, ctx->font);
        *ctx += new AnimatedText(Color::Blue, "I am blue fat text!", 0.75f, 0.5f, ctx->font);


        ctx->texts[0]->SetPos(8, 0).SetColor(Color::White).SetOutlineColor(Color::Lime).CenterInBounds(boundsLeft, boundsTop, boundsWidth, boundsHeight);
        reinterpret_cast<AnimatedText *>(ctx->texts[1])->SetPos(16, 200);

        float text2PosX = 400.0f - 16.0f - ctx->texts[2]->GetWidth(); // right-justify
        reinterpret_cast<AnimatedText *>(ctx->texts[2])->SetPos(text2PosX, 200);

        ctxs.push_back(ctx);
    };

    const char *fonts[] =
    {
        "sysfont", "ACNL_font.bcfnt", "Nasa.ttf", "fontawesome.ttf", "Roboto-Regular.ttf",
        "prod-sans.ttf", "grandhotel_2.ttf", "ZELDA Regular.ttf", "PressStart2P.ttf",
        "HACKED.ttf", "retganon.ttf", "The Wild Breath of Zelda.otf", "Triforce.ttf"
    };

    for (const char *str : fonts)
        initCtxs(str);

    currentCtx = ctxs.begin();
}

static void sceneRender(float size, bool italic, bool outline)
{
    C2D_DrawRectSolid(boundsLeft, boundsTop, 0.5f, boundsWidth, boundsHeight, Color::Black.raw);
    // Draw static text strings
    if (currentCtx != ctxs.end() && (*currentCtx)->texts.size() >= 3)
    {
        u32  style = (italic ? ITALIC : 0) | (outline ? OUTLINE : 0);
        (*currentCtx)->texts[0]->SetSize(size, size).SetStyle(style).Draw(false);
        reinterpret_cast<AnimatedText *>((*currentCtx)->texts[1])->Draw(true);
        reinterpret_cast<AnimatedText *>((*currentCtx)->texts[2])->Draw(true);
    }

    // This code makes no sense, it just show the potential usage of operators with Text objects
    static Text     textSize("Current text size: ", 0.5f, 0.5f);
    static Text     currFont("Current font: ", 0.5f, 0.5f);

    (textSize + Format("%f (Use  to change)", size)).Draw(8, 220, true);

    Text&& myText = currFont + ((*currentCtx)->name + " (Use \uE079\uE07A to change)");

    myText.Draw(8, 235, true);
}

static void sceneExit(void)
{
    for (FontContext *ctx : ctxs)
        delete ctx;
    ctxs.clear();
}

int main()
{
    // Initialize the libs
    romfsInit();
    gfxInitDefault();
    C3D_Init(C3D_DEFAULT_CMDBUF_SIZE);
    C2D_Init(C2D_DEFAULT_MAX_OBJECTS);
    C2D_Prepare();

    // Create screen
    top = C2D_CreateScreenTarget(GFX_TOP, GFX_LEFT);
    bottom = C2D_CreateScreenTarget(GFX_BOTTOM, GFX_LEFT);

    // Initialize the scene
    sceneInit();

    float size = 0.5f;
    float fps;
    float outline = false;
    float italic = false;
    Clock clock;

    // Main loop
    while (aptMainLoop())
    {
        hidScanInput();
        fps = 1.f / AsSeconds(clock.Restart());

        // Respond to user input
        u32 kDown = hidKeysDown();
        u32 kHeld = hidKeysHeld();
        if (kDown & KEY_START)
            break; // break in order to return to hbmenu

        if (kHeld & KEY_L)
            size -= 1.0f/128.f;
        else if (kHeld & (KEY_R | KEY_ZR))
            size += 1.0f/128.f;
        else if (kHeld & KEY_X)
            size = 0.5f;
        else if (kHeld & KEY_Y)
            size = 1.0f;
        else if (kDown & KEY_DUP)
        {
            if (currentCtx != ctxs.begin())
                currentCtx--;
        }
        else if (kDown & KEY_DDOWN)
        {
            if (currentCtx + 1 != ctxs.end())
                currentCtx++;
        }
        else if (kDown & KEY_A)
            italic = !italic;
        else if (kDown & KEY_B)
            outline = !outline;

        if (size < 0.25f)
            size = 0.25f;
        else if (size > 2.0f)
            size = 2.0f;

        // Render the scene
        C3D_FrameBegin(C3D_FRAME_SYNCDRAW);

        // Render top screen
        C2D_TargetClear(top, C2D_Color32(0x68, 0xB0, 0xD8, 0xFF));
        C2D_SceneBegin(top);
        sceneRender(size, italic, outline);

        // Render bottom screen
        C2D_TargetClear(bottom, C2D_Color32(0x68, 0xB0, 0xD8, 0xFF));
        C2D_SceneBegin(bottom);
        float posY = Text(Format("CPU: %2.2f", C3D_GetProcessingTime() * 6.0f)).Draw(0, 0);
        posY = Text(Format("GPU: %2.2f", C3D_GetDrawingTime() * 6.0f)).Draw(0, posY);
        posY = Text(Format("CmdBuf: %2.2f", C3D_GetCmdBufUsage() * 100.0f)).Draw(0, posY);
        Text(Format("FPS: %2.2f", fps)).Draw(0, posY);

        C3D_FrameEnd(0);
    }

    // Deinitialize the scene
    sceneExit();

    // Deinitialize the libs
    C2D_Fini();
    C3D_Fini();
    gfxExit();
    return 0;
}
