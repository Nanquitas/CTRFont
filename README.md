# CTRFont

A simple lib to help rendering Text with different fonts using **Citro2d**  
Supports BCFNT and TrueType font loading (most type supported by FreeType should work)

## How-to

You can find an example of usage in the ``example`` folder  
It's basically the *3ds-example/system-font* example rewritten to use CTRFont

### Credits

This use part of code from ctrulib and citro2d so credit to their original creator
